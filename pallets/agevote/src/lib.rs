#![cfg_attr(not(feature = "std"), no_std)]
pub use pallet::*;
use pallet_timestamp::{self as timestamp};

#[frame_support::pallet]
pub mod pallet {
	use super::*;
	use frame_support::{pallet_prelude::*, BoundedBTreeSet};
	use frame_system::pallet_prelude::*;

	use codec::alloc::vec::Vec;
	use scale_info::prelude::string::String;
	use lite_json::{JsonValue, json_parser::parse_json};

	#[pallet::pallet]
	#[pallet::generate_store(pub(super) trait Store)]
	pub struct Pallet<T>(_);

	/* ========== PALLET CONFIGURATION ========== */
	
	#[pallet::config]
	pub trait Config: frame_system::Config + timestamp::Config {
		/// Because this pallet emits events, it depends on the runtime's definition of an event.
		type RuntimeEvent: From<Event<Self>> + IsType<<Self as frame_system::Config>::RuntimeEvent>;

		#[pallet::constant]
		/// Maximum length of the json representation of votes
		type MaxVoteJSONLength: Get<u32>;

		#[pallet::constant]
		/// Maximum length of an encrypted ballot
		type MaxEncryptedBallotLength: Get<u32>;

		#[pallet::constant]
		/// Maximum length of an encrypted Telling Token
		type MaxEncryptedTellingTokenLength: Get<u32>;

		#[pallet::constant]
		/// Maximum number of ballots for a vote
		type MaxBallots: Get<u32>;

		#[pallet::constant]
		/// Maximum number of nodes
		type MaxNodes: Get<u32>;

		#[pallet::constant]
		/// Minimum number of validations (results author excluded)
		type MinValidations: Get<u32>;

		#[pallet::constant]
		/// Maximum number of nodes
		type MaxResultsJSONLength: Get<u32>;
	}

	/* ========== TYPES & DATA STRUCTRES ========== */

	type VoteJson<T> = BoundedVec<u8, <T as Config>::MaxVoteJSONLength>;
	type EncryptedBallot<T> = BoundedVec<u8, <T as Config>::MaxEncryptedBallotLength>;
	type EncryptedBallots<T> = BoundedBTreeSet<EncryptedBallot<T>, <T as Config>::MaxBallots>;
	type EncryptedTellingToken<T> = BoundedVec<u8, <T as Config>::MaxEncryptedTellingTokenLength>;
	type EncryptedTellingTokens<T> = BoundedBTreeSet<EncryptedTellingToken<T>, <T as Config>::MaxBallots>;
	type ResultsJson<T> = BoundedVec<u8, <T as Config>::MaxResultsJSONLength>;
	type Validations<T> = BoundedBTreeSet<<T as frame_system::Config>::AccountId, <T as Config>::MaxNodes>;
	type VoteIndex = u32;
	type VoteStatus = u8;
	type VoteDuration = u32;

	#[derive(Clone, Encode, Decode, Eq, PartialEq, RuntimeDebug, Default, TypeInfo, MaxEncodedLen)]
	#[scale_info(skip_type_params(T))]
	pub struct VoteOnChainData<T: Config> {
		vote_json: VoteJson<T>,
		encrypted_ballots: EncryptedBallots<T>,
		encrypted_telling_tokens: EncryptedTellingTokens<T>,
		results: Option<ResultsJson<T>>,
		/// 0 prepared, 1 open, 2 closed, 3 validated, 4 canceled, 5 archived
		status: VoteStatus,
		validations: Validations<T>,
		opened_at: Option<T::Moment>,
		/// duration is parsed from vote_json, not directly provided
		duration: VoteDuration,
	}

	#[derive(Clone)]
	pub struct VoteJsonParsed {
		duration: u32
	}
	
	/* ========== STORAGE ========== */
	
	/// Storage map storing the votes
	#[pallet::storage]
	#[pallet::getter(fn votes_map)]
	pub type VotesMap<T: Config> = StorageMap<_, Twox64Concat, VoteIndex, VoteOnChainData<T>>;

	/// Trusted authors are the signers allowed to sign all extrinsics for AGEVoté
	#[pallet::storage]
	#[pallet::getter(fn trusted_authors)]
	pub type TrustedAuthors<T: Config> = StorageValue<_, BoundedVec<T::AccountId, T::MaxNodes>, ValueQuery>;

	/// Stores the list of the votes with status = Open (1) to watch for the automatic Closed (2) transition
	#[pallet::storage]
	#[pallet::getter(fn open_votes)]
	pub type OpenVotes<T: Config> = StorageValue<_, BoundedBTreeSet<VoteIndex, T::MaxNodes>, ValueQuery>;

	/* ========== GENESIS SETUP ========== */

	#[pallet::genesis_config]
	pub struct GenesisConfig<T: Config> {
		pub trusted_authors: Vec<T::AccountId>,
	}

	#[cfg(feature = "std")]
	impl<T: Config> Default for GenesisConfig<T> {
		fn default() -> Self {
			Self { trusted_authors: Vec::new() }
		}
	}

	#[pallet::genesis_build]
	impl<T: Config> GenesisBuild<T> for GenesisConfig<T> {
		fn build(&self) {
			if !self.trusted_authors.is_empty() {
				assert!(<TrustedAuthors<T>>::get().is_empty(), "Trusted authors are already initialized!");
				let bounded: BoundedVec<T::AccountId, T::MaxNodes> = self.trusted_authors.clone().try_into()
					.expect("Initial trusted authors set must be less than T::MaxNodes");
				<TrustedAuthors<T>>::put(bounded);
			}
		}
	}

	/* ========== UTILITY FONCTIONS ========== */

	/// This is a public call, so we ensure that the origin is one of the trusted authors
	fn ensure_trusted<T: Config>(origin: OriginFor<T>) -> Result<T::AccountId, Error<T>> {
		let who = ensure_signed(origin).map_err(|_| Error::<T>::Forbidden)?;
		ensure!(<TrustedAuthors<T>>::get().contains(&who), Error::<T>::Forbidden);
		Ok(who)
	}

	/// Parsing raw json bytes into a usable structure for fields used by the blockchain
	/// We use the lite_json crate for WASM compatibility.  
	fn parse_vote_json(bytes: Vec<u8>) -> Result<VoteJsonParsed, ()> {
		let json_str = String::from_utf8(bytes).map_err(|_| ())?;
		let json_str = json_str.replace(|c: char| !c.is_ascii(), ""); // workaround because lite_json can't handle non-ascii ...
		let vote_parsed: JsonValue = parse_json(&json_str).map_err(|_| ())?;
		let vote_parsed = vote_parsed.to_object().ok_or(())?;

		let (_, duration) = vote_parsed.iter().find(|(k, _)|
			k.iter().copied().eq("duration".chars())
		).ok_or(())?;
		let duration: u32 = duration.as_number().ok_or(())?.integer.try_into().map_err(|_| ())?;

		Ok(VoteJsonParsed {
			duration,
		})
	}

	/* ========== PALLET EVENTS ========== */

	#[pallet::event]
	#[pallet::generate_deposit(pub(super) fn deposit_event)]
	pub enum Event<T: Config> {
		/// A vote has been added with the given status (either open or closed). [vote_index, vote_json, status, who]
		VoteAdded { vote_index: VoteIndex, vote_json: VoteJson<T>, status: u8, who: T::AccountId },
		
		/// The status of a vote has been updated. It can be triggered by a manual (who = Some(AccountId)) or automatic transition (who = None). [vote_index, old_status, new_status, who]
		VoteStatusUpdated { vote_index: VoteIndex, old_status: VoteStatus, new_status: VoteStatus, who: Option<T::AccountId> },
		
		/// An encrypted ballot has been added to a vote so the telling token is broadcasted for identity verification by the tellers. [vote_index, encrypted_telling_token, who]
		VoteBallotAdded { vote_index: VoteIndex, encrypted_telling_token: EncryptedTellingToken<T>, who: T::AccountId },

		/// Results of the primary backend have been published and are available for validations by secondary backends. [vote_index, encrypted_ballots, results, who]
		VoteResultsAdded { vote_index: VoteIndex, encrypted_ballots: EncryptedBallots<T>, results: ResultsJson<T>, who: T::AccountId },

		/// A vote received a validation for its results
		VoteValidation { vote_index: VoteIndex, validations: Validations<T>, who: T::AccountId },
	
		/// The list of trusted authors has been updated
		TrustedAuthorsUpdated { previous_trusted_authors: Vec<T::AccountId>, new_trusted_authors: Vec<T::AccountId>, who: T::AccountId },
	}

	/* ========== PALLET ERRORS ========== */

	#[pallet::error]
	pub enum Error<T> {
		/// Something terrible happend
		Internal,

		/// Bad format for the input parameters (ex: Bad JSON)
		BadFormat,

		/// Value bigger than storage bounds.
		StorageOverflow,

		/// Index not found, already used or out of range
		BadIndex,

		/// Duplicate vote or ballot
		Duplicate,

		/// Action violates protocol
		Protocol,

		/// The signer is not a member of the trusted authors
		Forbidden,
	}

	/* ========== PALLET DISPATCHABLE FUNCTIONS ========== */

	// Dispatchable functions allows users to interact with the pallet and invoke state changes.
	// These functions materialize as "extrinsics", which are often compared to transactions.
	#[pallet::call]
	impl<T: Config> Pallet<T> {

		/// Adds a vote to the blockchain in a JSON encoded and initiate the related on-chain states.
		/// Its status can be either Prepared (0) or Open (1).
		#[pallet::call_index(0)]
		#[pallet::weight(T::DbWeight::get().reads_writes(1,1).ref_time())]
		pub fn add_vote(origin: OriginFor<T>, vote_index: VoteIndex, status: VoteStatus, vote_json: Vec<u8>) -> DispatchResult {
			let who = ensure_trusted::<T>(origin)?;

			// New votes' status must be either Prepared (0) or Open (1)
			ensure!(status == 0 || status == 1, Error::<T>::Protocol);
 
			let vote_json: VoteJson<T> =
				vote_json.try_into().map_err(|_| Error::<T>::StorageOverflow)?;

			let vote_json_parsed = parse_vote_json(vote_json.clone().into_inner()).map_err(|_| Error::<T>::BadFormat)?;
			let duration = vote_json_parsed.duration;

			if let Ok(_) = <VotesMap<T>>::try_get(vote_index) {
				// A vote with the same index already exists
				Err(Error::<T>::Duplicate.into())
			} else {
				let opened_at = match status { 1 => Some(<timestamp::Pallet<T>>::get()), _ => None };
				let vote = VoteOnChainData {
					vote_json: vote_json.clone(),
					encrypted_ballots: EncryptedBallots::<T>::default(),
					encrypted_telling_tokens: EncryptedTellingTokens::<T>::default(),
					results: None,
					status,
					validations: Validations::<T>::default(),
					opened_at,
					duration,
				};
				
				// If the vote status is Open (1) we record the timestamp
				if status == 1 {
					<OpenVotes<T>>::mutate(|votes| { votes.try_insert(vote_index).unwrap() }); // same size as the number of votes
				}

				<VotesMap<T>>::insert(vote_index, vote);
				Self::deposit_event(Event::VoteAdded {vote_index, vote_json, status, who});

				Ok(())
			}
		}

		/// Update the status of a vote. Conditions apply :
		/// - it must be >= to the current one
		/// - Canceled (4) and Archived (5) statuses are final
		///		Prepared  0 -> Open 1, Canceled 5
		/// 	Open 	  1 -> Canceled 5, AUTO: Closed 2
		/// 	Closed    2 -> AUTO: Validated 3
		/// 	Validated 3 -> Archived 4
		/// 	Archived  4 and Cancelled 5 are final
		/// (AUTO means only the runtime should perform the transition)
		#[pallet::call_index(1)]
		#[pallet::weight(T::DbWeight::get().reads_writes(1,1).ref_time())]
		pub fn update_status(origin: OriginFor<T>, vote_index: VoteIndex, new_status: u8) -> DispatchResult {
			let who = ensure_trusted::<T>(origin)?;

			// Statuses bound are between Prepared (0) and Archived (5).
			if new_status < 1 || new_status > 5 {
				return Err(Error::<T>::Protocol.into())
			}

			<VotesMap<T>>::mutate(vote_index, |vote| match vote {
				Some(vote) => {
					// Check if the desired manual status update is legal.
					let old_status = vote.status;
					ensure!(
						match old_status {
							0 => new_status == 1 || new_status == 5,
							1 => new_status == 5,
							3 => new_status == 4,
							_ => false
						}, Error::<T>::Protocol);
					
					vote.status = new_status;
					Self::deposit_event(Event::VoteStatusUpdated {vote_index, old_status, new_status, who: Some(who)});
					
					// If the vote is Open (1) we record the timestamp
					if new_status == 1 {
						vote.opened_at = Some(<timestamp::Pallet<T>>::get());
						<OpenVotes<T>>::mutate(|votes| { votes.try_insert(vote_index).unwrap() }); // same size as the number of votes
					}

					Ok(())
				},
				None => Err(Error::<T>::BadIndex.into())
			})
		}

		/// Add en encrypted ballot to an open vote
		#[pallet::call_index(2)]
		#[pallet::weight(T::DbWeight::get().reads_writes(1,1).ref_time())]
		pub fn add_encrypted_ballot(origin: OriginFor<T>, vote_index: VoteIndex, encrypted_ballot: Vec<u8>, encrypted_telling_token: Vec<u8>) -> DispatchResult {
			let who = ensure_trusted::<T>(origin)?;

			let encrypted_ballot: EncryptedBallot<T> =
				encrypted_ballot.try_into().map_err(|_| Error::<T>::StorageOverflow)?;

			let encrypted_telling_token: EncryptedTellingToken<T> =
				encrypted_telling_token.try_into().map_err(|_| Error::<T>::StorageOverflow)?;

			<VotesMap<T>>::mutate(vote_index, |vote| match vote {
				Some(vote) => {
					// Only accept ballots if the vote is open
					ensure!(vote.status == 1, Error::<T>::Protocol);

					// An identical ballot already exists (and has almost surely been added by the same user)
					let duplicate = vote.encrypted_ballots.contains(&encrypted_ballot)
						|| vote.encrypted_telling_tokens.contains(&encrypted_telling_token); // tokens check is a sanity check
					ensure!(!duplicate, Error::<T>::Duplicate);

					vote.encrypted_ballots.try_insert(encrypted_ballot).map_err(|_| Error::<T>::StorageOverflow)?;
					vote.encrypted_telling_tokens.try_insert(encrypted_telling_token.clone()).map_err(|_| Error::<T>::StorageOverflow)?;
					
					Self::deposit_event(Event::VoteBallotAdded { vote_index, encrypted_telling_token, who });
					Ok(())
				},
				None => Err(Error::<T>::BadIndex.into())
			})
		}

		#[pallet::call_index(3)]
		#[pallet::weight(T::DbWeight::get().reads_writes(1,1).ref_time())]
		pub fn add_results(origin: OriginFor<T>, vote_index: VoteIndex, results: Vec<u8>) -> DispatchResult {
			let who = ensure_trusted::<T>(origin)?;

			let results: ResultsJson<T> = results.try_into().map_err(|_| Error::<T>::StorageOverflow)?;

			<VotesMap<T>>::mutate(vote_index, |vote| match vote {
				Some(vote) => {
					// Only Closed (2) votes should receive results
					ensure!(vote.status == 2, Error::<T>::Protocol);

					// Results are immutable
					ensure!(vote.results.is_none(), Error::<T>::Protocol);

					vote.results = Some(results.clone());
					vote.validations.try_insert(who.clone()).map_err(|_| Error::<T>::Internal)?;

					let encrypted_ballots = vote.encrypted_ballots.clone();
					Self::deposit_event(Event::VoteResultsAdded { vote_index, encrypted_ballots, results, who });
					Ok(())
				},
				None => Err(Error::<T>::BadIndex.into())
			})
		}

		#[pallet::call_index(4)]
		#[pallet::weight(T::DbWeight::get().reads_writes(1,1).ref_time())]
		pub fn validate_results(origin: OriginFor<T>, vote_index: VoteIndex) -> DispatchResult {
			let who = ensure_trusted::<T>(origin)?;

			<VotesMap<T>>::mutate(vote_index, |vote| match vote {
				Some(vote) => {
					// Only Closed (2) votes should receive validations
					ensure!(vote.status == 2, Error::<T>::Protocol);

					let inserted = vote.validations.try_insert(who.clone()).map_err(|_| Error::<T>::StorageOverflow)?;
					ensure!(inserted, Error::<T>::Duplicate);

					Self::deposit_event(Event::VoteValidation { vote_index, validations: vote.validations.clone(), who: who.clone() });
					if vote.validations.len() == usize::try_from(T::MinValidations::get()).map_err(|_| Error::<T>::Internal)? {
						vote.status = 3;
						Self::deposit_event(Event::VoteStatusUpdated {vote_index, old_status: 2, new_status: 3, who: Some(who)});
					}
					Ok(())
				},
				None => Err(Error::<T>::BadIndex.into())
			})
		}

		/// Replace the current set of trusted authors be a new one. The signer is automatically added to prevent self-locking
		#[pallet::call_index(5)]
		#[pallet::weight(T::DbWeight::get().reads_writes(1,1).ref_time())]
		pub fn edit_trusted_authors(origin: OriginFor<T>, trusted_authors: Vec<T::AccountId>) -> DispatchResult {
			let who = ensure_trusted::<T>(origin)?;
			let previous_trusted_authors = <TrustedAuthors<T>>::get();
			
			// Ensure the signer is in the new set
			let mut trusted_authors = trusted_authors;
			if !trusted_authors.contains(&who) {
				trusted_authors.push(who.clone())
			}

			let bounded: BoundedVec<T::AccountId, T::MaxNodes> = trusted_authors.clone().try_into().map_err(|_| Error::<T>::StorageOverflow)?;
			<TrustedAuthors<T>>::put(bounded);
			Self::deposit_event(Event::TrustedAuthorsUpdated { previous_trusted_authors: previous_trusted_authors.to_vec(), new_trusted_authors: trusted_authors, who });
			Ok(())
		}
	}

	/* ========== RUNTIME HOOKS ========== */

	#[pallet::hooks]
	impl<T: Config> Hooks<BlockNumberFor<T>> for Pallet<T> {
		fn on_finalize(_: T::BlockNumber) {
			let now = <timestamp::Pallet<T>>::get();
			
			for vote_index in <OpenVotes<T>>::get() {
				<VotesMap<T>>::mutate(vote_index, |vote| match vote {
					Some(vote) => {
						if vote.status == 1 {
							let opened_at = vote.opened_at.expect("Open vote with opened_at = None !");
							if now.cmp(&opened_at) == core::cmp::Ordering::Greater {
								let delta: Result<u32, _> = (now - opened_at).try_into();
								let Ok(delta) = delta else { panic!("Failed to convert timestamp::Moment (now - opened_at) to u32") };
					
								if delta >= vote.duration {
									vote.status = 2;
									Self::deposit_event(Event::VoteStatusUpdated {vote_index, old_status: 1, new_status: 2, who: None});
									<OpenVotes<T>>::mutate(|votes| { votes.remove(&vote_index) });
								}
							}
						} else {
							// This case occurs when a vote is archived while open
							<OpenVotes<T>>::mutate(|votes| { votes.remove(&vote_index) });
						} 
					},
					None => panic!("Orphan vote_index in OpenVotes !")
				})
			}
		}
	}
}
