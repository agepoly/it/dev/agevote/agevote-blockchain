#!/usr/bin/bash
# This script is meant to be run inside the production container

set -e
# Allows agevote-wipe-script to kill PID 1
trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT SIGHUP

# Waiting for setup-done signal
SETUP_DONE=/data/setup-done
printf "Waiting for initial setup...\n >> Please refer to the instructions at https://gitlab.com/agepoly/it/dev/agevote/agevote-deployment/\n"
while [ ! -f "$SETUP_DONE" ]; do
    sleep 5
done
echo "Initial setup OK"

cd /data
READY_FOR_RESTART=/data/ready-for-restart
URL="${CHAIN_SPEC_URL:=https://gitlab.com/agepoly/it/dev/agevote/agevote-deployment/-/raw/main/configuration/agevote-spec.json}"
while true; do
    
    # Fetching the chain specification from deployment repository
    printf "Downloading latest chain specification from $URL\n"
    wget $URL -O chain-spec.json

    # Printing useful info from chain spec
    bootnodes=$( jq '.bootNodes' /data/chain-spec.json )
    printf "Loaded chain specification with bootnodes : $bootnodes\n\n"

    # Starting blockchain node
    /usr/local/bin/node-agevote \
        --base-path /data/node-data \
        --chain /data/chain-spec.json \
        --node-key-file /data/node-key.txt \
        --port 30333 \
        --ws-port 9944 \
        --rpc-cors all \
        --unsafe-ws-external \
        --validator

    printf "\n\n>>> Blockchain node exited, waiting for the restart signal ...\n"

    # Waiting for ready-for-restart signal
    while [ ! -f "$READY_FOR_RESTART" ]; do
        echo "ee"
        sleep 1
    done
    rm $READY_FOR_RESTART
    echo "Ready-for-restart signal received !"
done