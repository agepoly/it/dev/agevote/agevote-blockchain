#!/usr/bin/bash
# This script is meant to be run inside the production container

set -e
printf "*** AGEVOTE-SETUP ***\n"

SETUP_DONE=/data/setup-done
if [[ -f "$SETUP_DONE" && ($1 != "force") ]]; then
   printf "Blockchain node already initialized. Force new setup by appending 'force'\n\n"
   cat /data/my-keys.txt
   exit 0
fi

echo "Initializing ..."

node-agevote key generate --scheme Sr25519 --output-type json > /data/sr25519.json
secret_phrase=$( jq '.secretPhrase' /data/sr25519.json )
secret_hex=$( jq '.secretSeed' /data/sr25519.json | tr -d '"' )
public_sr25519=$( jq '.ss58PublicKey' /data/sr25519.json | tr -d '"' )

node-agevote key inspect --scheme Ed25519 --output-type json $secret_hex > /data/ed25519.json
public_ed25519=$( jq '.ss58PublicKey' /data/ed25519.json | tr -d '"' )

printf "$secret_phrase" > /data/secret-phrase.txt
printf "$secret_hex" > /data/secret-hex.txt

node-agevote key generate-node-key --file /data/node-key.txt 2> /dev/null
peer_id=$( node-agevote key inspect-node-key --file /data/node-key.txt )

printf "Secret phrase       : $secret_phrase\n" > /data/my-keys.txt
printf "Peer ID             : $peer_id\n" >> /data/my-keys.txt
printf "Sr25519 public key  : $public_sr25519\n" >> /data/my-keys.txt
printf "Edr25519 public key : $public_ed25519\n" >> /data/my-keys.txt

node-agevote key insert --base-path /data/node-data --scheme Sr25519 --suri $secret_hex --key-type aura
node-agevote key insert --base-path /data/node-data --scheme Ed25519 --suri $secret_hex --key-type gran

printf "All done ! Content of /data/my-keys.txt :\n"
cat /data/my-keys.txt

touch $SETUP_DONE