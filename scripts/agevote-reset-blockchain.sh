#!/usr/bin/bash
# This script is meant to be run inside the production container

# Kill the blockchain process (it will wait for the ready-to-restart signal)
pkill node-agevote

# Delete the rocksdb database but preserve the keystore
rm -r /data/node-data/chains/agevote_epfl/db/

# Ready for restart
touch /data/ready-for-restart