#!/usr/bin/bash
# This script is meant to be run inside the production container

# Kill the blockchain process (it will wait for the ready-to-restart signal)
pkill node-agevote

# Delete everything in the /data
rm -r /data/*

# Kill the entrypoint to exit the container (PID 1 protection is trapped by entrypoint)
kill 1