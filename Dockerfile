# This is an example build stage for the node template. Here we create the binary in a temporary image.

FROM docker.io/paritytech/ci-linux:production as builder
WORKDIR /node-agevote
COPY . .
RUN cargo build --locked --release

FROM docker.io/library/ubuntu:20.04
RUN apt update && apt install -y wget curl vim jq
# Copy the scripts
COPY --from=builder /node-agevote/scripts/agevote-setup.sh /usr/local/bin/agevote-setup
COPY --from=builder /node-agevote/scripts/agevote-reset-blockchain.sh /usr/local/bin/agevote-reset-blockchain
COPY --from=builder /node-agevote/scripts/agevote-wipe-all.sh /usr/local/bin/agevote-wipe-all
COPY --from=builder /node-agevote/scripts/agevote-entrypoint.sh /usr/local/bin/agevote-entrypoint
# Copy the executable
COPY --from=builder /node-agevote/target/release/node-agevote /usr/local/bin
RUN mkdir /data && \
  chmod +x /usr/local/bin/agevote-* && \
  /usr/local/bin/node-agevote --version

EXPOSE 30333 9933 9944 9615
VOLUME ["/data"]
ENTRYPOINT ["/usr/local/bin/agevote-entrypoint"]